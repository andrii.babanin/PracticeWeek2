﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MathGame.Internal;

namespace MathGame
{
    public  enum MathGameOperation
    {
        Addition,
        Subtraction
    }

    public class MathGame : IMathGame
    {
        private int _score;
        private CancellationTokenSource _tokenSource;
        private IMathGameStrategy _strategy;

        public Task StartGame(StrategyType strategy)
        {
            _score = 0;
            _strategy = StrategyProvider.GetStrategy(strategy);

            Console.WriteLine("Game has been started\nSelected strategy : {0}", _strategy.StrategyName);
            _tokenSource = new CancellationTokenSource();

            var playTask = Task.Run(() => Play(_tokenSource.Token));

            playTask.Wait(1000 * 5);
            _tokenSource.Cancel();

            return Task.CompletedTask;
        }

        private Task Play(CancellationToken token)
        {

            try
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    var task = _strategy.CreateTask();
                    Console.WriteLine(task.Message);

                    var answer = Convert.ToInt32(Console.ReadLine());

                    var response = _strategy.ReceiveRoundInformation(task, answer);
                    ProcessResponse(response);
                }
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine("Time has been elapsed");
                StopGame();
            }


            return Task.CompletedTask;
        }
        public Task StopGame()
        {
            ShowGameResult();

            return Task.CompletedTask;
        }

        private void ShowGameResult()
        {
            Console.WriteLine("Game is finished. Your score : {0}", _score);
        }

        private void ProcessResponse(MathGameResult result)
        {
            if (!result.IsCorrect)
            {
                _tokenSource.Cancel();
                return;
            }

            _score += result.ScoreChanges;
        }
    }
}
