﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathGame
{
    public interface IMathGameStrategy
    {
        public string StrategyName { get; set; }

        public MathGameTask CreateTask();
        public MathGameResult ReceiveRoundInformation(MathGameTask task, int answer);
    }
}
