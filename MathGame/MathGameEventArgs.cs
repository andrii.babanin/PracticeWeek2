﻿using System;

namespace MathGame
{
    public class MathGameEventArgs : EventArgs
    {
        public MathGameTask Task { get; private set; }
        public int Answer { get; private set; }

        public MathGameEventArgs(MathGameTask task, int answer)
        {
            Task = task;
            Answer = answer;
        }
    }
}
