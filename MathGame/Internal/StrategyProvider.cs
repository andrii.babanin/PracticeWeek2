﻿using MathGame.Internal.Strategy;

namespace MathGame.Internal
{
    internal class StrategyProvider
    {
        public static IMathGameStrategy GetStrategy(StrategyType type)
        {
            return type switch
            {
                _ => new DefaultStrategy()
            };
        }
    }
}
