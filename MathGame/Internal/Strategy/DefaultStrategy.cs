﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathGame.Internal.Strategy
{
    internal class DefaultStrategy : IMathGameStrategy
    {
        private readonly TaskCreator _taskCreator;
        public string StrategyName { get; set; }

        public DefaultStrategy()
        {
            StrategyName = "Default";
            _taskCreator = new TaskCreator();
        }

        public MathGameTask CreateTask()
        {
            return _taskCreator.CreateTask(2);
        }

        public MathGameResult ReceiveRoundInformation(MathGameTask task, int answer)
        {
            var expectedAnswer = ExpressionCalculator.CalculateCorrectAnswer(task.Operands,task.Operations);

            if (expectedAnswer != answer)
            {
                return new MathGameResult()
                {
                    ContinueGame = false,
                    IsCorrect = false,
                    ScoreChanges = 0
                };
            }

            return new MathGameResult()
            {
                ContinueGame = true,
                IsCorrect = true,
                ScoreChanges = 1
            };
        }
    }
}
