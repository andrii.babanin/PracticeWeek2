﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MathGame.Internal
{
    internal class TaskCreator
    {
        private readonly Dictionary<MathGameOperation, char> operationDictionary = new Dictionary<MathGameOperation, char>()
        {
            {MathGameOperation.Addition, '+'},
            {MathGameOperation.Subtraction, '-'}
        };

        public  MathGameTask CreateTask(int operandsCount)
        {
            if (operandsCount <= 1)
                throw new ArgumentException();

            var rnd = new Random(DateTime.Now.Millisecond);

            var operandsList = new List<int>();
            for (int i = 0; i < operandsCount; i++)
            {
                operandsList.Add(rnd.Next(0 , 50));    
            }

            var operationsList = new List<MathGameOperation>();
            for (int i = 0; i < operandsCount - 1; i++)
            {
                operationsList.Add(
                    MathOperationUtils.GetOperation(rnd.Next(0 ,50)));
            }


            return new MathGameTask()
            {
                Operations = operationsList,
                Operands = operandsList,
                Message = CreateMessage(operandsList, operationsList).ToArray()
            };
        }

        private  Span<char> CreateMessage(List<int> operands, List<MathGameOperation> operations)
        {
            var stepCount = operands.Count();

            var builder = new StringBuilder(stepCount * 2);

            for (var i = 0; i < stepCount - 1; i++)
            {
                builder.Append(operands[i]);
                builder.Append(operationDictionary[operations[i]]);
            }

            builder.Append(operands[^1]);

            var output = new Span<char>(new char[builder.Length]);
            builder.CopyTo(0, output, builder.Length);

            return output;
        }

    }
}
