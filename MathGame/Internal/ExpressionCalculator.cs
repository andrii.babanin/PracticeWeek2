﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathGame.Internal
{
    internal class ExpressionCalculator
    {
        public static int CalculateCorrectAnswer(IEnumerable<int> operands, IEnumerable<MathGameOperation> operations)
        {
            //Here you can implement calculator which uses revers polish notation to correctly calculate complex math expressions

            //Just a simple calculator realization that can use addition and subtraction
            
            var operandsQueue = new Queue<int>(operands);
            var operationsQueue = new Queue<MathGameOperation>(operations);
            var calculationCount = operationsQueue.Count;

            var calcResult = operandsQueue.Dequeue();
            for (int i = 0; i < calculationCount; i++)
            {
                var operand = operandsQueue.Dequeue();
                var operation = operationsQueue.Dequeue();

                switch (operation)
                {
                    case MathGameOperation.Addition:
                        calcResult += operand;
                        break;
                    case MathGameOperation.Subtraction:
                        calcResult -= operand;
                        break;
                }
            }

            return calcResult;
        }
    }
}
