﻿namespace MathGame.Internal
{
    internal class MathOperationUtils
    {
        public static MathGameOperation GetOperation(int number)
        {
            return number switch
            {
                1 => MathGameOperation.Subtraction,
                _ => MathGameOperation.Addition
            };
        }
    }
}
