﻿using System.Threading.Tasks;

namespace MathGame
{
    public interface IMathGame
    {
        Task StartGame(StrategyType strategy);
        Task StopGame();
    }
}
