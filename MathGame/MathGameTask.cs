﻿using System;
using System.Collections.Generic;

namespace MathGame
{
    public  class MathGameTask
    {
        public IEnumerable<int> Operands { get; set; }
        public IEnumerable<MathGameOperation> Operations { get; set; }
        //Does char array better in this situation ?
        public char[] Message { get; set; }
    }
}
