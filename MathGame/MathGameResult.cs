﻿namespace MathGame
{
    public class MathGameResult
    {
        public bool IsCorrect { get; set; }
        public bool ContinueGame { get; set; }
        public int ScoreChanges { get; set; }
    }
}
