﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestProj
{
    class GenericContainer<T> where T : IComparable
    {
        public IEnumerable<T> Collection;

        public GenericContainer(IEnumerable<T> collection)
        {
            Collection = collection;
        }
    }
}
