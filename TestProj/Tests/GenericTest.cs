﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProj.Tests
{
    class GenericTest
    {
        public static void Start()
        {
            Console.WriteLine("\nGeneric Tasks Section");
            var container = new GenericContainer<Message>(
                new List<Message>()
                {
                    new Message("FirstMessage", DateTime.Parse("13:30")),
                    new Message("SecondMessage", DateTime.Parse("13:00"))
                });

            container.Collection = container.Collection.OrderBy(x => x);

            var collection = container.Collection.ToList();

            foreach (var message in collection)
            {
                Console.WriteLine(message.Name);
            }
            int totalMinutes = Convert.ToInt32(collection.ElementAt(0));
        }
    }
}
