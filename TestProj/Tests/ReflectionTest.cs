﻿using System;
using System.Linq;
using System.Reflection;
using TestProj.CustomAttribute;

namespace TestProj.Tests
{
    class ReflectionTest
    {
        public static void Start()
        {
            Console.WriteLine("\nReflection Tasks Section");
            var classWithAttributes = new ClassWithAttributes();
            var classMethods = classWithAttributes.GetType().GetMethods()
                .Where(x => x.GetCustomAttributes(typeof(MyCustomAttribute), false).Length > 0);

            foreach (var method in classMethods)
            {
                method.Invoke(classWithAttributes, null);
            }

            //Section above must print in console two lines:
            //"Hello" and "Greetings"

            //private generic method from static class ClassWithGenerics

            var genericMethod = typeof(ClassWithGenerics).GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .FirstOrDefault(x => x.IsGenericMethod)?.MakeGenericMethod(typeof(int));
            genericMethod?.Invoke(null, new object?[1] { 34 });
            //Section above must print in console "34"
        }
    }
}
