﻿using System;
using System.Collections.Generic;
using System.Text;
using TestProj.Events;

namespace TestProj.Tests
{
    class EventsTest
    {
        public static void Start()
        {
            var receiver = new Receiver();
            receiver.NewSpecialMessage += SpecialMessageHandler;

            var defaultMessage = new DefaultMessage() { From = "A", Time = DateTime.Now, Title = "T" };
            var specialMessage = new SpecialMessage()
                { Title = "T", Time = DateTime.Now, SpecialField = "This is a special message!" };

            receiver.ReceiveMessage(defaultMessage);
            receiver.ReceiveMessage(specialMessage);
        }

        public static void SpecialMessageHandler(object? sender, SpecialMessageEventArgs e)
        {
            Console.WriteLine("Handle special field : {0}", e.SpecialMessage);
        }
    }
}
