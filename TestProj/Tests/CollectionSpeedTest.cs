﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TestProj.Tests
{
    class CollectionSpeedTest
    {
        public static void Start()
        {
            Console.WriteLine("\nCollection Tasks Section");
            int collectionSize = 1000 * 1000 * 100;

            MeasureTimeArray(collectionSize);
            MeasureTimeList(collectionSize);
        }

        static void MeasureTimeArray(int collectionSize)
        {
            var arrayCollection = new int[collectionSize];

            var watch = new Stopwatch();
            watch.Start();
            for (int i = 0; i < collectionSize; i++)
            {
                var t = arrayCollection[i];
            }
            watch.Stop();

            Console.WriteLine("For loop in array took {0} seconds", watch.Elapsed);
            watch.Reset();

            watch.Start();
            foreach (var i in arrayCollection) ;
            watch.Stop();
            Console.WriteLine("Foreach loop int array took {0} seconds\n", watch.Elapsed);
        }

        static void MeasureTimeList(int collectionSize)
        {
            var listCollection = new List<int>(collectionSize);

            for (int i = 0; i < collectionSize; i++)
                listCollection.Add(0);
            var watch = new Stopwatch();

            watch.Start();
            for (int i = 0; i < collectionSize; i++)
            {
                var t = listCollection[i];
            }
            watch.Stop();

            Console.WriteLine("For loop in list took {0} seconds", watch.Elapsed);
            watch.Reset();

            watch.Start();
            foreach (var i in listCollection) ;
            if (watch != null)
            {
                watch.Stop();
                Console.WriteLine("Foreach loop int list took {0} seconds\n", watch.Elapsed);
            }
        }
    }
}
