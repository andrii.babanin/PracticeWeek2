﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;

namespace TestProj
{
    public class DataTypesSolution
    {
        private readonly List<string> _adviceList = new List<string>()
        {
            "Keep it simple? stupid",
            "This is a very wise advice",
            "Another wise advice",
            "Just an ordinary advice"
        };

        public enum SalaryDivideType
        {
            PerYear,
            PerWeek,
            PerHour,
            PerMinute
        };

        public double DivideSalary(double monthSalary, SalaryDivideType divideType)
        {
            return divideType switch
            {
                SalaryDivideType.PerHour => monthSalary / (30d * 24),
                SalaryDivideType.PerMinute => monthSalary / (30 * 24 * 60),
                SalaryDivideType.PerWeek => monthSalary / (4d),
                SalaryDivideType.PerYear => monthSalary * 12d,
                _ => default
            };
        }

        public string  GetRandomAdvice()
        {
            var rnd = new Random(DateTime.Now.Millisecond);

            var index = rnd.Next(0, _adviceList.Count - 1);

            return _adviceList[index];
        }

        public void Test()
        {
            var a = _adviceList;
        }
    }
}
