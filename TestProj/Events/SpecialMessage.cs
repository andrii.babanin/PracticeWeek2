﻿using System;

namespace TestProj.Events
{
    public class SpecialMessageEventArgs : EventArgs
    {
        public string SpecialMessage { get; private set; }

        public SpecialMessageEventArgs(string message)
        {
            SpecialMessage = message;
        }

    }
    public class SpecialMessage : MessageBase
    {
        public string SpecialField { get; set; }
    }
}
