﻿namespace TestProj.Events
{
    class DefaultMessage : MessageBase
    {
        public string From { get; set; }
    }
}
