﻿using System;

namespace TestProj.Events
{
    public class Receiver
    {
        public event EventHandler<SpecialMessageEventArgs> NewSpecialMessage = (sender, args) => { };
        public void ReceiveMessage(dynamic message)
        {
            switch (message)
            {
                case DefaultMessage defaultMessage:
                    Console.WriteLine("Just a default message");
                    break;
                case SpecialMessage specialMessage:
                    NewSpecialMessage(this, new SpecialMessageEventArgs(specialMessage.SpecialField));
                    break;
            }
        }
    }
}
