﻿using System;

namespace TestProj.Events
{
    public abstract class MessageBase
    {
        public string Title { get; set; }
        public DateTime Time { get; set; }
    }
}
