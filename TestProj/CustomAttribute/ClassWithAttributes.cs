﻿using System;

namespace TestProj.CustomAttribute
{
    class ClassWithAttributes
    {
        [MyCustom]
        public void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        public void PrintHelloWorld()
        {
            Console.WriteLine("Hello World");
        }

        [MyCustom]
        public void PrintGreetings()
        {
            Console.WriteLine("Greetings!");
        }
    }
}
