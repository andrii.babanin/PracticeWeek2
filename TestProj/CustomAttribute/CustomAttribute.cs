﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestProj.CustomAttribute
{

    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Method)]
    class MyCustomAttribute : System.Attribute
    {
        public bool IsCustom;

        public MyCustomAttribute()
        {
            IsCustom = true;
        }
    }
}
