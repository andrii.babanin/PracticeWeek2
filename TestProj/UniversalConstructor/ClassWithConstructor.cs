﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestProj.UniversalConstructor
{
    public class ClassWithConstructor
    {
        public int Value { get; }
        private ClassWithConstructor()
        {
            Value = 1234;
        }

        public ClassWithConstructor(int value)
        {
            Value = value;
        }
    }
}
