﻿using System;
using System.Linq;
using System.Reflection;

namespace TestProj.UniversalConstructor
{
    public class UniConstructor
    {
        //If I "cache" already used types in collection like Dictionary[TypeName , Type] will it increase performance ?? 
        //or how to store predefined types

        public static object Construct<T>() where T : class
        {
            var type = typeof(T);

            if (!type.IsClass)
                return null;

            var availableConstructors = type.GetConstructors(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.GetParameters().Length == 0)
                .ToList();

            //Activator.CreateInstance<T>();
            //Activator.CreateInstance(typeof(T), null);
            return availableConstructors.Count == 0 ? null : availableConstructors[0].Invoke(null);
        }
    }
}
