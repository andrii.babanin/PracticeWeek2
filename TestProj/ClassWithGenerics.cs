﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace TestProj
{
    public static class ClassWithGenerics
    {
        private static void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        private static void PrintGeneric<T>(T something)
        {
            Console.WriteLine(something.ToString());
        }
    }
}
