﻿using System;
using System.Text;

namespace TestProj.CryptoTask
{
    class Encryptor
    {
        public static string Encrypt(string target, int key)
        {
            if (String.IsNullOrEmpty(target))
                throw new NullReferenceException();

            var targetBytes = Encoding.UTF8.GetBytes(target);

            for (int i = 0; i < targetBytes.Length; i++)
            {
                targetBytes[i] = (byte)((int)targetBytes[i] ^ key);
            }

            return Encoding.UTF8.GetString(targetBytes);
        }
    }
}
