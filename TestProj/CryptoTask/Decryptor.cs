﻿using System.Text;

namespace TestProj.CryptoTask
{
    class Decryptor
    {
        public static string Decrypt(string encryptedString, int key)
        {
            var encryptedBytes = Encoding.UTF8.GetBytes(encryptedString);

            for (int i = 0; i < encryptedBytes.Length; i++)
            {
                encryptedBytes[i] = (byte) ((int) encryptedBytes[i] ^ key);
            }

            return Encoding.UTF8.GetString(encryptedBytes);
        }
    }
}
