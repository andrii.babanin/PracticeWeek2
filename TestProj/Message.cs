﻿using System;
using System.Globalization;

namespace TestProj
{
    /// <summary>
    /// Task 1: 2)Create a custom class that implements the necessary interfaces to allow an array of the class to be sorted.
    /// Task 2: 3)Create a custom class that can be converted to common value types.
    /// </summary>
    public class Message : IComparable, IConvertible
    {
        public string Name { get; set; }
        public DateTime Time { get; set;}

        public Message()
        {
            Name = default;
            Time = default;
        }

        public Message(string name, DateTime time)
        {
            Name = name;
            Time = time;
        }

        public int CompareTo(object? obj)
        {
            if (obj is null) return 1;

            var anotherMessage = obj as Message;

            if (anotherMessage != null)
                return this.Time.CompareTo(anotherMessage.Time);
            else
                throw new ArgumentException("Wrong type of object");
        }

        #region TypeConvertionRegion

        public TypeCode GetTypeCode()
        {
            return TypeCode.Object;
        }

        public bool ToBoolean(IFormatProvider? provider)
        {
            return false;
        }

        public byte ToByte(IFormatProvider? provider)
        {
            return (byte)Time.Hour;
        }

        public char ToChar(IFormatProvider? provider)
        {
            return ' ';
        }

        public DateTime ToDateTime(IFormatProvider? provider)
        {
            return Time;
        }

        public decimal ToDecimal(IFormatProvider? provider)
        {
            return Time.Hour * 60 + Time.Minute;
        }

        public double ToDouble(IFormatProvider? provider)
        {
            return Time.Hour * 60 + Time.Minute;
        }

        public short ToInt16(IFormatProvider? provider)
        {
            return (short)(Time.Hour * 60 + Time.Minute);
        }

        public int ToInt32(IFormatProvider? provider)
        {
            return Time.Hour * 60 + Time.Minute;
        }

        public long ToInt64(IFormatProvider? provider)
        {
            return Time.Hour * 60 + Time.Minute;
        }

        public sbyte ToSByte(IFormatProvider? provider)
        {
            return (sbyte)Time.Hour;
        }

        public float ToSingle(IFormatProvider? provider)
        {
            throw new NotImplementedException();
        }

        public string ToString(IFormatProvider? provider)
        {
            return Time.ToString(CultureInfo.InvariantCulture);
        }

        public object ToType(Type conversionType, IFormatProvider? provider)
        {
            return Convert.ChangeType(this.Time, conversionType);
        }

        public ushort ToUInt16(IFormatProvider? provider)
        {
            return (ushort)(Time.Hour * 60 + Time.Minute);
        }

        public uint ToUInt32(IFormatProvider? provider)
        {
            return (uint)(Time.Hour * 60 + Time.Minute);
        }

        public ulong ToUInt64(IFormatProvider? provider)
        {
            return (ulong)(Time.Hour * 60 + Time.Minute);
        }

        #endregion

    }
}
